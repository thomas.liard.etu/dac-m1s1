terraform {
required_version = ">= 0.13.4"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.35.0"
    }
  }
}

resource "openstack_compute_keypair_v2" "keypair" {
  name       = "terakey"
  public_key = file("~/.ssh/terakey.pub")
}

resource "openstack_compute_instance_v2" "ubuntuInsttances" {
  count           = 4
  name            = "twoTerraformInstancesUbuntu"
  provider        = openstack
  image_name      = "ubuntu-20.04"
  flavor_name     = "normale"
  key_pair        = "terakey"
}

